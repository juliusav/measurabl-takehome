# For Measurabl Takehome Assignment


## NOTES:

I chose AngularJS as the front-end web framework

**Why:**

* I’ve been using AngularJS with my current company for the last 8 years, so I’m most comfortable with this framework for this assignment
* I haven’t set up a project file structure like this since about 8 years ago, so I looked online to find a quick way to set it up.  
* I found this article which explained how to use yeoman to scaffold the AngularJS app: https://medium.com/bitmaker-software/scaffolding-a-new-angularjs-project-db01151f16e0 

**How:**

* Using yeoman, I created the AngularJS project with the following constraints:
    * Minimal external libs (bootstrap, jquery, etc.)
    * LESS — CSS preprocessor
    * Karma test runner
* Due to AngularJS being older and not well supported, the prerequisites are a older versions of Gulp and Node:
    * Gulp 3.9 or earlier
    * Node 11 or earlier (locally, I've been using v8.12.0)
* Unfortunately I was not able to get the local unit test runner working, so I added comments under the unit test files with all of the things I would have tested

**Instructions:**

* Make sure you have Gulp < 3.9 and Node < 11
* Download and unzip the directory
* `cd` into the directory
* Run `gulp serve` — this should run the local server and the app should open in browser
