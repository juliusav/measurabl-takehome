(function() {
  'use strict';

  describe('controller list view', function() {
    var ctrl;
    var deferred;
    var dataProvider;

    beforeEach(module('measurablTakehome'));
    beforeEach(inject(function($compile, $rootScope, _dataProvider_, $q, $controller) {
      deferred = $q.defer();

      dataProvider = _dataProvider_;
      spyOn(dataProvider, 'getAgesAndNames').and.callFake(function() {
        return deferred.promise;
      });

      ctrl = $controller('ListViewCtrl');
    }));

    describe('ListViewCtrl init', function() {
      //agesAndNames should be set to empty array
      //it should call dataProvider.getAgesAndNames()
      //
      //dataProvider.getAgesAndNames success callback
      //  it should set agesAndNames based on return param
      //    test falsy values
      //    test diff types
      //  it should set agesAndNames to empty array if return param is invalid
      //
      //dataProvider.getAgesAndNames error callback
      //  it should log error
    });

  });
})();
