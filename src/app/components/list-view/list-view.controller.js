(function() {
  'use strict';

  angular
    .module('measurablTakehome')
    .directive('listView', listView);

  /** @ngInject */
  function listView() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/list-view/list-view.html',
      scope: { },
      controller: ListViewCtrl,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function ListViewCtrl(dataProvider, $log) {
      var vm = this;
      vm.agesAndNames = [];

      $log.log('ListViewCtrl initiated');
      dataProvider.getAgesAndNames()
        .then(function(agesAndNames) {
          vm.agesAndNames = agesAndNames || [];
          $log.log('vm.agesAndNames:',vm.agesAndNames);
        })
        .catch(function(err) {
          $log.error('error retrieving ages and names', err);
        });
      
    }
  }

})();
