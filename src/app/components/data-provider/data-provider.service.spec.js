(function() {
  'use strict';

  describe('service dataProvider', function() {
    var dataProvider;

    beforeEach(module('measurablTakehome'));
    beforeEach(inject(function(_dataProvider_) {
      dataProvider = _dataProvider_;
    }));

    describe('getAges function', function() {
      //it should call $http.get on agesEndpoint
      //$http.get success callback
      //  if response data is valid
      //     it should set ages based on response data 
      //     it should return promise
      //  if response data is invalid
      //     it should set ages to empty array 
      //     it should return promise
    });

    describe('getNames function', function() {
      //it should call $http.get on namesEndpoint
      //$http.get success callback
      //  if response data is valid
      //     it should set names based on response data 
      //     it should return promise
      //  if response data is invalid
      //     it should set names to empty array 
      //     it should return promise
    });

    describe('getAgesAndNames function', function() {
      //it should call getAges()
      //getAges() success callback
      //  ages should be equal to returnedAges param
      //    test different values / types for param
      //getAges() error callback
      //  it should log error
      //it should call getNames()
      //getNames() success callback
      //  it should call mergeAgesAndNames
      //  it should return promise that resolves with agesAndNames
      //    test different return values for mergeAgesAndNames 
      //getNames() error callback
      //  it should log error
    });

    describe('mergeAgesAndNames function', function() {
      //it should return empty array if ages param is invalid
      //it should return empty array if names param is invalid
      //it should return array of namesAndAges objects
      //  test different values for ages param
      //  test different values for names param
      //it should have array elements ordered by id
    });

    describe('convertIdToInt function', function() {
      //1. it should set item.id to parsed int
      
      //unit tests if assumption of valid input were removed:
      //  - test for falsy params
      //  - test different types for item.id
    });
  });
})();
