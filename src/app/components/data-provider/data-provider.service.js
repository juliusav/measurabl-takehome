(function() {
  'use strict';

  angular
    .module('measurablTakehome')
    .factory('dataProvider', dataProvider);

  /** @ngInject */
  function dataProvider($log, $http, $q) {
    var agesEndpoint = 'http://5c37c33f7820ff0014d927c5.mockapi.io/msr/ages';
    var namesEndpoint = 'http://5c37c33f7820ff0014d927c5.mockapi.io/msr/names';

    var service = {
      getAges: getAges,
      getNames: getNames,
      getAgesAndNames: getAgesAndNames,
      mergeAgesAndNames: mergeAgesAndNames
    };

    return service;

    function getAges() {
      $log.log('retrieving ages');

      var deferred = $q.defer();

      $http.get(agesEndpoint)
        .then(function(resp) {
          var ages;

          if (resp && resp.data) {
            $log.log('successfully retrieved ages:', resp.data);
            ages = resp.data;
          } else {
            ages = []
            $log.warn('invalid return for ages', resp.data);
          }

          deferred.resolve(ages);
        })
        .catch(function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }

    function getNames() {
      $log.log('retrieving names');

      var deferred = $q.defer();

      $http.get(namesEndpoint)
        .then(function(resp) {
          var names;

          if (resp && resp.data) {
            $log.log('successfully retrieved names:', resp.data);
            names = resp.data;
          } else {
            names = []
            $log.warn('invalid return for names', resp.data);
          }

          deferred.resolve(names);
        })
        .catch(function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }

    function getAgesAndNames() {
      var ages;

      var deferred = $q.defer();

      service.getAges()
        .then(function(returnedAges) {
          ages = returnedAges;
        })
        .then(service.getNames)
        .then(function(returnedNames) {
          var agesAndNames = service.mergeAgesAndNames(ages, returnedNames);
          deferred.resolve(agesAndNames);
        })
        .catch(function(err) {
          $log.error('error in getAgesAndNames', err);
        });

      return deferred.promise;
    }

    function mergeAgesAndNames(ages, names) {
      if (!ages || !names) {
        $log.warn('mergeAgesAndNames - invalid params...', 'ages: ' + ages, 'names: ' + names);
        return [];
      }

      var map = new Map();

      ages.forEach(function(item) {
        convertIdToInt(item);
        map.set(item.id, item); 
      });

      names.forEach(function(item) {
        convertIdToInt(item);
        var mergedItem = Object.assign({}, map.get(item.id) || {}, item);
        map.set(item.id, mergedItem);
      });

      //Convert map to array for ng-repeat in html template
      var agesAndNamesArr = Array.from(map).map(function(item) {
        return item[1];
      });

      return agesAndNamesArr;
    }

    //ID as int is used for ordering of list
    function convertIdToInt(item) {
      item.id = parseInt(item.id); //Assumption: item.id always exists
    }
  }
})();
