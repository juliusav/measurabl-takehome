(function() {
  'use strict';

  angular
    .module('measurablTakehome', ['ui.router', 'toastr']);

})();
