(function() {
  'use strict';

  angular
    .module('measurablTakehome')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
